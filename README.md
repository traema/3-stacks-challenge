# 3-stacks-challenge

## Getting started

## To run 

```shell
make run
```

with docker (check you current in the makefile)

```shell
make drun
```


## To test 

```shell
make tests
```

with docker (check you current in the makefile)

```shell
make dtests
```