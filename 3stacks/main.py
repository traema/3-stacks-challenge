from service.ThreeStacks import ThreeStacks

def fpush(obj, stack, elt):
    obj.push(stack, elt)
    obj.show()


def main():
    print('hello_world')
    mystack = ThreeStacks()
    mystack.show()


    fpush(mystack, 2, 55)
    fpush(mystack, 2, 56)
    print(mystack.pop(2))
    mystack.show()

    fpush(mystack, 1, 53)
    fpush(mystack, 1, 52)
    print(mystack.pop(1))
    mystack.show()

    fpush(mystack, 3, 33)
    fpush(mystack, 3, 32)
    print(mystack.pop(3))
    mystack.show()
    fpush(mystack, 3, 100)
    print(mystack.pop(3))
    

if __name__ == "__main__":
    main()