from unittest import TestCase
from service.ThreeStacks import ThreeStacks


class TestThreeStacks(TestCase):

    def test_process_ok(self):
        nstacks = ThreeStacks()
 
        nstacks.push(2, 15)
        nstacks.push(2, 45)

        nstacks.push(1, 17)
        nstacks.push(1, 49)
        nstacks.push(1, 39)

        nstacks.push(3, 11)
        nstacks.push(3, 5)
        nstacks.push(3, 7)

        self.assertEqual(nstacks.pop(2), 45)
        self.assertEqual(nstacks.pop(1), 39)
        self.assertEqual(nstacks.pop(3), 7)
        self.assertEqual(nstacks.pop(2), 15)
        self.assertEqual(nstacks.pop(1), 49)

    def test_monostack_1(self):
        self.monostack(1)

    def test_monostack_2(self):
        self.monostack(2)
    
    def test_monostack_1(self):
        self.monostack(2)

    def monostack(self, stacknum):
        nstacks = ThreeStacks()
 
        nstacks.push(stacknum, 15)
        nstacks.push(stacknum, 45)
        nstacks.push(stacknum, 17)
        nstacks.push(stacknum, 49)
        nstacks.push(stacknum, 39)
        nstacks.push(stacknum, 11)
        nstacks.push(stacknum, 5)
        nstacks.push(stacknum, 7)

        self.assertEqual(nstacks.pop(stacknum), 7)
        self.assertEqual(nstacks.pop(stacknum), 5)
        self.assertEqual(nstacks.pop(stacknum), 11)
        self.assertEqual(nstacks.pop(stacknum), 39)
        self.assertEqual(nstacks.pop(stacknum), 49)

    def test_with_string(self):
        nstacks = ThreeStacks()
 
        nstacks.push(2, 'toto')
        nstacks.push(2, 'roger')

        nstacks.push(1, 17)
        nstacks.push(1, 'batman')
        nstacks.push(1, 39)

        nstacks.push(3, 11)
        nstacks.push(3, 'mars')
        nstacks.push(3, 'pluto')

        self.assertEqual(nstacks.pop(2), 'roger')
        self.assertEqual(nstacks.pop(1), 39)
        self.assertEqual(nstacks.pop(3), 'pluto')
        self.assertEqual(nstacks.pop(2), 'toto')
        self.assertEqual(nstacks.pop(1), 'batman')