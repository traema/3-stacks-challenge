
class ThreeStacks:

    def __init__(self):
        #todo: use dynamical arrays
        self.theOnlyOneArray = [0 for i in range(150)]
        
    def push(self, stack_num, value):
        self.theOnlyOneArray[stack_num-1] += 1
        self.theOnlyOneArray[(3 * self.theOnlyOneArray[stack_num-1]) + (stack_num - 1)] =value
        
    def pop(self, stack_num):
        if self.is_empty(stack_num):
            return None
        value = self.theOnlyOneArray[(3 * self.theOnlyOneArray[stack_num-1]) + (stack_num - 1)]
        self.theOnlyOneArray[stack_num-1] -= 1
        return value
    
    def is_empty(self, stack_num):
        return self.theOnlyOneArray[stack_num-1] == 0

    def show(self):
        print(self.theOnlyOneArray)
    