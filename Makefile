project_dir = ./3stacks
APP_NAME = ${project_dir}/main.py
current_dir = ${CURDIR}

tests:
	cd ${project_dir}/ && python -m unittest

run:
	python $(APP_NAME)

drun:
	docker run --rm -it -v $(current_dir):/usr/src/app/ -w /usr/src/app --entrypoint python python:3.9 $(APP_NAME)

dtests:
	docker run --rm -it -v $(current_dir):/usr/src/app/ -w /usr/src/app/${project_dir} --entrypoint python python:3.9 "-m unittest"